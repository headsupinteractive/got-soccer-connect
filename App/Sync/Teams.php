<?php

namespace GotSoccer\App\Sync;

use GotSoccer\App\Sync;
use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Teams
 *
 * @package GotSoccer\App\Sync
 */
class Teams extends Sync implements WordPressHooks
{
    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        // add_action('wp_ajax_got_soccer_sync_events', [$this, 'syncEventsAjax']);
    }

    public function syncTeams()
    {
        $page_size = 1000;

        $get_teams = $this->Api->GetEventTeams(
            $EventID = null,
            $GroupID = null,
            $BracketID = null,
            $ClubID = null,
            $ApplicationID = null,
            $TeamID = null,
            $Gender = null,
            $AgeGroup = null,
            $PageSize = $page_size,
            $Page = 1
        );

        if (!$get_teams) {
            return false;
        }

        $log = $this->startLog('gotsoccer-teams.log', 'w+');
        fwrite($log, "Page: 1\r\n");

        // Get total pages
        $pages = isset($get_teams->GetEventTeamsSummary->TotalPages) ? (int) $get_teams->GetEventTeamsSummary->TotalPages : 1;
        $total = isset($get_teams->GetEventTeamsSummary->TotalRows) ? (int) $get_teams->GetEventTeamsSummary->TotalRows : '-1';
        $updated = $this->buildTeams($get_teams->GetEventTeamsResults->GetEventTeamsResult, $log);

        if ($total <= $page_size) {
            echo sprintf('Teams Updated: %1$s/%2$s', $updated, $total);
            return;
        }

        // Loop for each page
        for ($i = 2; $i <= $pages; $i++) {
            $get_teams = $this->Api->GetEventTeams(
                $EventID = null,
                $GroupID = null,
                $BracketID = null,
                $ClubID = null,
                $ApplicationID = null,
                $TeamID = null,
                $Gender = null,
                $AgeGroup = null,
                $PageSize = $page_size,
                $Page = $i
            );

            fwrite($log, "\r\nPage: ".$i."\r\n");
            $updated += $this->buildTeams($get_teams->GetEventTeamsResults->GetEventTeamsResult, $log);
        }

        echo sprintf('Teams Updated: %1$s/%2$s', $updated, $total);
    }

    private function buildTeams($teams, $log)
    {
        if (empty($teams)) {
            return false;
        }

        $count = 0;
        $teams_ranked = $this->calculateRanks($teams);

        foreach ($teams as $team) {
            $team_id       = !empty((array) $team->TeamID) ? $team->TeamID : null;
            $age           = !empty((array) $team->GroupAge) ? $team->GroupAge : null;
            $gender        = isset($team->Gender) ? $team->Gender : null;
            $group         = isset($team->GroupName) ? $team->GroupName : null;
            $bracket       = isset($team->BracketName) ? $team->BracketName : null;
            $event_name    = isset($team->EventName) ? $team->EventName : null;
            $team_event_id = !empty((array) $team->EventID) ? $team->EventID : null;
            $team_name     = isset($team->TeamNameFull) ? $team->TeamNameFull : null;

            if (!$team_id) {
                fwrite($log, $team_id . ": Skipped\r\n");
                continue;
            }

            // Set rank if found
            $key_rank = array_search($team_name, array_column($teams_ranked[$age][$gender][$group][$bracket], 'team'));
            if (isset($teams_ranked[$age][$gender][$group][$bracket][$key_rank]['rank'])) {
                $team->Rank = $teams_ranked[$age][$gender][$group][$bracket][$key_rank]['rank'];
            }
            if (isset($teams_ranked[$age][$gender][$group][$bracket][$key_rank]['ranktot'])) {
                $team->RankTot = $teams_ranked[$age][$gender][$group][$bracket][$key_rank]['ranktot'];
            }

            $post_type = 'got-team';
            $post_name = !empty($event_name) ? $event_name . ' | ' . $team_name : $team_name;
            $post_id   = $this->existingRowHandler($post_type, 'team_id', $team_id, 'event_id', $event_id);

            // Skip updating post if it exists already and isn't part of the active league
            if ($post_id !== 0 && $this->active_event_id !== $team_event_id) {
                continue;
            }
                
            $post_meta = $this->createPostMeta($team);
            $insert_id = $this->insertPost($post_id, $post_name, $post_type, $team_id, $post_meta);

            if (!is_wp_error($insert_id)) {
                fwrite($log, $team_id.": ".$post_name."\r\n");
                $count++;
            } else {
                fwrite($log, $team_id.": ".$post_name." (error) ".json_encode($insert_id)."\r\n");
            }
        }

        update_option('gotsoccer_sync_teams', time(), false);
        return $count;
    }

    private function calculateRanks($teams) {
        $teams_ranked = [];

        if (empty($teams)) {
            return $teams_ranked;
        }

        foreach ($teams as $team) {
            $age       = isset($team->GroupAge) && !empty((array) $team->GroupAge) ? $team->GroupAge : null;
            $gender    = isset($team->Gender) ? $team->Gender : null;
            $group     = isset($team->GroupName) ? $team->GroupName : null;
            $bracket   = isset($team->BracketName) ? $team->BracketName : null;
            $team_name = isset($team->TeamNameFull) ? $team->TeamNameFull : null;
            $points    = isset($team->GroupPlayPoints) && !empty((array) $team->GroupPlayPoints) ? $team->GroupPlayPoints : null;

            if ($age === null || $gender === null || $group === null || $bracket === null || $team_name === null || $points === null) {
                continue;
            }

            $rank       = 1;
            $rank_tied  = 1;
            $score_prev = -1;

            $teams_ranked[$age][$gender][$group][$bracket][] = [
                'team'   => $team_name,
                'points' => $points,
                'rank'   => 0,
                'ranktot' => 0
            ];

            foreach ($teams_ranked[$age][$gender][$group][$bracket] as $key => $team_rank) {
                $teams_ranked[$age][$gender][$group][$bracket][$key]['ranktot'] = count($teams_ranked[$age][$gender][$group][$bracket]);

                if ($teams_ranked[$age][$gender][$group][$bracket][$key]['points'] !== $score_prev) { // Not tie

                    $score_prev = $teams_ranked[$age][$gender][$group][$bracket][$key]['points'];
                    $rank = $rank_tied;
                    $teams_ranked[$age][$gender][$group][$bracket][$key]['rank'] = $rank_tied++;

                } else { // Tie

                    $teams_ranked[$age][$gender][$group][$bracket][$key]['rank'] = $rank;
                    $rank_tied++;

                }
            }
        }

        return $teams_ranked;
    }
}