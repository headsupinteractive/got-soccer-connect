<?php

namespace GotSoccer\App;

/**
 * Class Api
 *
 * @package GotSoccer\App
 */
class Api
{

    private $api_url;
    private $api_org_id;
    private $api_login;
    private $api_password;

    public function __construct()
    {
        $orgid = get_option('gotsoccer_orgid');
        $orgid = !empty($orgid) ? $orgid : null;
        $login = get_option('gotsoccer_login');
        $login = !empty($login) ? $login : null;
        $pass  = get_option('gotsoccer_pass');
        $pass  = !empty($pass) ? $pass : null;

        if (!$orgid || !$login || !$pass) {
            return false;
        }

        $this->api_url = 'https://api2.gotsport.com/api/League.asmx/';
        $this->api_org_id = 'OrgID='.$orgid;
        $this->api_login = 'Login='.$login;
        $this->api_password = 'Password='.$pass;
        $this->api_creds = implode('&', [$this->api_org_id, $this->api_login, $this->api_password]);
    }

    private function getData($endpoint, $args = null)
    {
        set_time_limit(0);

        $data_url = ($this->api_url) . $endpoint;
        $data_fields = $this->api_creds . '&' . $args;

        $ch = curl_init();
        $options = array(
            CURLOPT_URL            => $data_url,
            CURLOPT_FAILONERROR    => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT        => 60,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data_fields,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        if (isset($response) && !empty($response)) {
            $response = new \SimpleXMLElement($response);
        }
        curl_close($ch);
        return json_decode(json_encode($response));
        die();
    }

    public function getClubDetail()
    {
    }

    public function getClubTeams(
        $ClubID = null,
        $TeamID = null,
        $EventID = null,
        $Gender = null,
        $AgeGroup = null,
        $PageSize = 1000,
        $Page = 1
    ) {
        $endpoint = 'GetClubTeams';

        $args = implode('&', ['ClubID='.$ClubID, 'TeamID='.$TeamID, 'EventID='.$EventID, 'Gender='.$Gender, 'AgeGroup='.$AgeGroup, 'PageSize='.$PageSize, 'Page='.$Page]);

        $teams = $this->getData($endpoint, $args);

        return $this->checkApiResponse($teams, $endpoint) ? $teams : false;
    }

    public function getClubs(
        $ClubSearch = null,
        $ClubZipCode = null,
        $ClubZipCode2 = null,
        $PageSize = 1000,
        $Page = 1
    ) {
        $endpoint = 'GetClubs';

        $args = implode('&', ['ClubSearch='.$ClubSearch, 'ClubZipCode='.$ClubZipCode, 'ClubZipCode='.$ClubZipCode2, 'PageSize='.$PageSize, 'Page='.$Page]);

        $clubs = $this->getData($endpoint, $args);

        return $this->checkApiResponse($clubs, $endpoint) ? $clubs : false;
    }

    public function getEventGroups()
    {
    }

    public function getEventSchedule(
        $EventID = null,
        $GroupID = null,
        $BracketID = null,
        $ClubID = null,
        $ApplicationID = null,
        $TeamID = null,
        $FieldID = null,
        $MatchID = null,
        $Gender = null,
        $AgeGroup = null,
        $FromDate = null,
        $ToDate = null,
        $PageSize = 1000,
        $Page = 1
    ) {
        $endpoint = 'GetEventSchedule';

        $args = implode('&', ['EventID='.$EventID, 'GroupID='.$GroupID, 'BracketID='.$BracketID, 'ClubID='.$ClubID, 'ApplicationID='.$ApplicationID, 'TeamID='.$TeamID, 'FieldID='.$FieldID, 'MatchID='.$MatchID, 'Gender='.$Gender, 'AgeGroup='.$AgeGroup, 'FromDate='.$FromDate, 'ToDate='.$ToDate, 'PageSize='.$PageSize, 'Page='.$Page]);

        $events = $this->getData($endpoint, $args);

        return $this->checkApiResponse($events, $endpoint) ? $events : false;
    }

    public function getEventTeams(
        $EventID = null,
        $GroupID = null,
        $BracketID = null,
        $ClubID = null,
        $ApplicationID = null,
        $TeamID = null,
        $Gender = null,
        $AgeGroup = null,
        $PageSize = 1000,
        $Page = 1
    ) {
        $endpoint = 'GetEventTeams';

        $args = implode('&', ['EventID='.$EventID, 'GroupID='.$GroupID, 'BracketID='.$BracketID, 'ClubID='.$ClubID,'ApplicationID='.$ApplicationID, 'TeamID='.$TeamID, 'Gender='.$Gender, 'AgeGroup='.$AgeGroup, 'PageSize='.$PageSize, 'Page='.$Page]);

        $eventTeams = $this->getData('GetEventTeams', $args);

        return $this->checkApiResponse($eventTeams, $endpoint) ? $eventTeams : false;
    }

    public function getEvents(
        $EventType = null,
        $EventYear = null,
        $PageSize = 1000,
        $Page = 1
    ) {
        $endpoint = 'GetEvents';

        $args = implode('&', ['EventType='.$EventType, 'EventYear='.$EventYear, 'PageSize='.$PageSize, 'Page='.$Page]);

        $events = $this->getData('GetEvents', $args);

        return $this->checkApiResponse($events, $endpoint) ? $events : false;
    }

    private function checkApiResponse($response, $prefix)
    {
        if ($response && !empty((array) $response) && property_exists($response, $prefix.'Summary') && property_exists($response, $prefix.'Results')
        ) {
            return true;
        } else {
            return false;
        }
    }
}
