<?php

use GotSoccer\App\Api;

$orgid  = get_option('gotsoccer_orgid');
$orgid  = !empty($orgid) ? $orgid : null;
$login  = get_option('gotsoccer_login');
$login  = !empty($login) ? $login : null;
$pass   = get_option('gotsoccer_pass');
$pass   = !empty($pass) ? $pass : null;
$season = get_option('gotsoccer_season');
$season = !empty($season) ? $season : null;
$season_label = get_option('gotsoccer_slabel');
$season_label = !empty($season_label) ? $season_label : null;

$gotconnected = false;

if (isset($orgid) && isset($login) && isset($pass)) {
    $GotSoccerApi = new Api();
    $get_events   = $GotSoccerApi->getEvents();

    if ($get_events && !empty((array) $get_events) && !empty((array) $get_events->GetEventsResults) && !empty((array) $get_events->GetEventsResults->GetEventsResult)) {
        $seasons = [];
        $events  = $get_events->GetEventsResults->GetEventsResult;

        foreach ($events as $key => $event) {
            $eventType = strtolower((string) $event->EventType);
            $eventID   = (int) $event->EventID;
            $eventName = (string) $event->EventName;

            if ($eventType !== 'league' || array_key_exists($eventID, $seasons)) {
                continue;
            }

            $seasons[$eventID] = $eventName;
        }

        if (!empty($seasons)) {
            $gotconnected = true;
        }
    }
}

?>

<div class="wrap got-soccer-wrap<?php echo $gotconnected === true ? ' got-connected' : ''; ?>">
    <h1><?php _e('GotSoccer Settings', 'got-soccer'); ?></h1>
    <form id="gotsoccersettings" method="post" name="post" class="got-soccer-form">
        <input type="hidden" name="action" value="got_soccer_settings" />
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="orgid"><?php _e('Organization ID', 'got-soccer'); ?></label>
                    </th>
                    <td>
                        <input name="orgid" id="orgid" type="text" class="regular-text" value="<?php echo esc_html($orgid); ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="login"><?php _e('Login', 'got-soccer'); ?></label>
                    </th>
                    <td>
                        <input name="login" id="login" type="text" class="regular-text" value="<?php echo esc_html($login); ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="pass"><?php _e('Password', 'got-soccer'); ?></label>
                    </th>
                    <td>
                        <input name="pass" id="pass" type="text" class="regular-text" value="<?php echo esc_html($pass); ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="gotnotconnected">
            <p class="submit">
                <span class="submit-button">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Connect', 'got-soccer'); ?>">
                    <span class="spinner"></span>
                </span>
                <span class="submit-status"><?php _e('Not Connected', 'got-soccer'); ?></span>
            </p>
        </div>
        <div id="gotconnected">
            <hr>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="season"><?php _e('Active Season', 'got-soccer'); ?></label>
                        </th>
                        <td>
                            <select name="season" id="season" class="regular-text">
                                <?php
                                foreach ($seasons as $season_id => $season_name) {
                                    $selected = '';
                                    if ((int) $season === (int) $season_id) {
                                        $selected = 'selected';
                                    }
                                    echo '<option value="'.esc_attr($season_id).'" '.$selected.'>'.esc_html($season_name).'</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="slabel"><?php _e('Season Label', 'got-soccer'); ?></label>
                        </th>
                        <td>
                            <input name="slabel" id="slabel" type="text" class="regular-text" value="<?php echo esc_html($season_label); ?>" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <span class="submit-button">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Save Changes', 'got-soccer'); ?>">
                    <span class="spinner"></span>
                </span>
                <span class="submit-status"><?php _e('Connected', 'got-soccer'); ?></span>
            </p>
        </div>
    </form>
</div>