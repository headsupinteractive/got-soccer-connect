<?php

namespace GotSoccer\App;

use GotSoccer\App\Api;
use GotSoccer\App\Sync\Clubs;
use GotSoccer\App\Sync\Events;
use GotSoccer\App\Sync\Matches;
use GotSoccer\App\Sync\Teams;

/**
 * Class Sync
 *
 * @package GotSoccer\App
 */
class Sync
{

    protected $Api;
    protected $event_id;

    public function __construct()
    {
        $this->Api = new Api();
        $this->event_id = get_option('gotsoccer_season');
    }

    public function syncAll()
    {
        (new Clubs())->syncClubs();
        (new Events())->syncEvents();
        (new Matches())->syncMatches();
        (new Teams())->syncTeams();
        die();
    }

    protected function startLog($filename, $mode)
    {
        $upload_dir = wp_upload_dir();
        $log_path   = $upload_dir['basedir'].'/'.$filename;
        $log        = fopen($log_path, $mode);
        date_default_timezone_set('America/Chicago');
        fwrite($log, "\r\nStart Sync: ".date('r')."\r\n");
        return $log;
    }

    protected function existingRowHandler($post_type, $key, $value, $key2 = null, $value2 = null)
    {
        global $wpdb;

        $secondary_join = !empty($key2) && !empty($value2) ? 'INNER JOIN wp_postmeta AS mt1 ON (wp_posts.ID = mt1.post_id)' : '';
        $secondary_and  = !empty($key2) && !empty($value2) ? 'AND ( mt1.meta_key = "'.esc_sql($key2).'" AND mt1.meta_value = "'.esc_sql($value2).'" )' : '';

        $sql = "SELECT wp_posts.ID
        FROM wp_posts
        INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id)
        {$secondary_join}
        WHERE 1=1
        AND wp_posts.post_type = '{$post_type}'
        AND wp_postmeta.meta_key = '{$key}'
        AND wp_postmeta.meta_value = '{$value}'
        {$secondary_and}
        LIMIT 1";

        // ADD LOGIC FOR 2ND POSTMETA SEARCH

        $existing_row = $wpdb->get_results($sql);
        $existing_row = !empty((array) $existing_row) ? $existing_row[0] : null;
        $existing_row = !empty((array) $existing_row) ? $existing_row->ID : 0;
        
        return $existing_row;
    }

    protected function insertPost($post_id, $post_title, $post_type, $post_name, $post_meta)
    {

        if ((empty($post_id) && $post_id !== 0) || empty($post_title) || empty($post_type) || empty($post_name) || empty($post_meta)) {
            return false;
        }

        $post_args = array(
            'ID'              => $post_id,
            'post_title'      => $post_title,
            'post_status'     => 'publish',
            'post_type'       => $post_type,
            'comment_status'  => 'closed',
            'ping_status'     => 'closed',
            'post_name'       => $post_name,
            'meta_input'      => $post_meta
        );

        return wp_insert_post($post_args, true);
    }

    protected function createPostMeta($object = null)
    {
        if (!$object) {
            return false;
        }

        $post_meta = [];

        foreach ($object as $field => $value) {
            if (strtolower($field) === 'row') {
                continue;
            }

            $field = $this->slugify($field);
            $value = !empty((array) $value) ? $value : null;

            $post_meta[$field] = $value;
        }

        return $post_meta;
    }

    protected function slugify($string)
    {
        if (!$string) {
            return false;
        }

        $string = strtolower(preg_replace('%([a-z])([A-Z])%', '\1_\2', $string));

        return $string;
    }

    protected function gotSoccerSettings()
    {

        $data = $_POST;

        foreach ($data as $key => $value) {
            if ($key === 'action') {
                continue;
            }

            $update_field = update_option($this->prefix.$key, $value, false);

            echo $update_field;
        }

        wp_die();
    }
}
