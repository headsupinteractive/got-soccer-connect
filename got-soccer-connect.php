<?php
/**
 * Plugin Name: GotSoccer Connect
 * Version: 1.2.0
 * Description: GotSoccer API Connection Handler
 * Author: Heads Up Interactive
 * Author URI: http://www.headsupinteractive.com
 *
 * @package GotSoccer
 */

use GotSoccer\App\Core\Init;
use GotSoccer\App\Setup;
use GotSoccer\App\Scripts;
use GotSoccer\App\Database;
use GotSoccer\App\Sync;

define('GOTSOCCER_VERSION', '1.2.0');
define('GOTSOCCER_API_DIR', untrailingslashit(dirname(__FILE__)));
define('GOTSOCCER_API_URL', plugin_dir_url(__FILE__));
define('GOTSOCCER_FILE', __FILE__);

require_once GOTSOCCER_API_DIR . '/vendor/autoload.php';

add_action('after_setup_theme', function () {
    (new Init())
        ->add(new Setup())
        ->add(new Scripts())
        ->add(new Database())
        ->initialize();

    new Sync();
});
