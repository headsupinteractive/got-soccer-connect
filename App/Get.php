<?php

namespace GotSoccer\App;

/**
 * Class Get
 *
 * @package GotSoccer\App
 */
class Get
{
    public function getGotPosts($post_type = null)
    {

        if (empty($post_type)) {
            return false;
        }

        $args = [
            'post_type'      => $post_type,
            'posts_per_page' => 100,
            'no_found_rows'  => true
        ];

        $posts = get_posts($args);

        return $posts;
    }
}
