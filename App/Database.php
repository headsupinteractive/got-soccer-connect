<?php

namespace GotSoccer\App;

use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Database
 *
 * @package GotSoccer\App
 */
class Database implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        register_activation_hook(GOTSOCCER_FILE, ['GotSoccer', 'createTables']);
    }

    public function createPostTypes()
    {
        $create_leagues     = $this->createLeagues();
        $create_tournaments = $this->createTournaments();
        $create_clubs       = $this->createClubs();
        $create_matches     = $this->createMatches();
        $create_teams       = $this->createTeams();
    }

    private function createLeagues()
    {
        $labels = [
            'name'               => _x('Leagues', 'post type general name', 'got-soccer'),
            'singular_name'      => _x('League', 'post type singular name', 'got-soccer'),
            'menu_name'          => _x('Leagues', 'admin menu', 'got-soccer'),
            'name_admin_bar'     => _x('League', 'add new on admin bar', 'got-soccer'),
            'add_new'            => _x('Add New', 'league', 'got-soccer'),
            'add_new_item'       => __('Add New League', 'got-soccer'),
            'new_item'           => __('New League', 'got-soccer'),
            'edit_item'          => __('Edit League', 'got-soccer'),
            'view_item'          => __('View League', 'got-soccer'),
            'all_items'          => __('All Leagues', 'got-soccer'),
            'search_items'       => __('Search Leagues', 'got-soccer'),
            'parent_item_colon'  => __('Parent Leagues:', 'got-soccer'),
            'not_found'          => __('No leagues found.', 'got-soccer'),
            'not_found_in_trash' => __('No leagues found in Trash.', 'got-soccer')
        ];
        $args = [
            'labels'             => $labels,
            'description'        => __('Description.', 'got-soccer'),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => ['slug' => 'league'],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 91,
            'supports'           => ['title', 'editor', 'custom-fields']
        ];
        register_post_type('got-league', $args);
    }

    private function createTournaments()
    {
        $labels = [
            'name'               => _x('Tournaments', 'post type general name', 'got-soccer'),
            'singular_name'      => _x('Tournament', 'post type singular name', 'got-soccer'),
            'menu_name'          => _x('Tournaments', 'admin menu', 'got-soccer'),
            'name_admin_bar'     => _x('Tournament', 'add new on admin bar', 'got-soccer'),
            'add_new'            => _x('Add New', 'league', 'got-soccer'),
            'add_new_item'       => __('Add New Tournament', 'got-soccer'),
            'new_item'           => __('New Tournament', 'got-soccer'),
            'edit_item'          => __('Edit Tournament', 'got-soccer'),
            'view_item'          => __('View Tournament', 'got-soccer'),
            'all_items'          => __('All Tournaments', 'got-soccer'),
            'search_items'       => __('Search Tournaments', 'got-soccer'),
            'parent_item_colon'  => __('Parent Tournaments:', 'got-soccer'),
            'not_found'          => __('No tournaments found.', 'got-soccer'),
            'not_found_in_trash' => __('No tournaments found in Trash.', 'got-soccer')
        ];
        $args = [
            'labels'             => $labels,
            'description'        => __('Description.', 'got-soccer'),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => ['slug' => 'tournament'],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 92,
            'supports'           => ['title', 'editor', 'custom-fields']
        ];
        register_post_type('got-tournament', $args);
    }

    private function createClubs()
    {
        $labels = [
            'name'               => _x('Clubs', 'post type general name', 'got-soccer'),
            'singular_name'      => _x('Club', 'post type singular name', 'got-soccer'),
            'menu_name'          => _x('Clubs', 'admin menu', 'got-soccer'),
            'name_admin_bar'     => _x('Club', 'add new on admin bar', 'got-soccer'),
            'add_new'            => _x('Add New', 'league', 'got-soccer'),
            'add_new_item'       => __('Add New Club', 'got-soccer'),
            'new_item'           => __('New Club', 'got-soccer'),
            'edit_item'          => __('Edit Club', 'got-soccer'),
            'view_item'          => __('View Club', 'got-soccer'),
            'all_items'          => __('All Clubs', 'got-soccer'),
            'search_items'       => __('Search Clubs', 'got-soccer'),
            'parent_item_colon'  => __('Parent Clubs:', 'got-soccer'),
            'not_found'          => __('No clubs found.', 'got-soccer'),
            'not_found_in_trash' => __('No clubs found in Trash.', 'got-soccer')
        ];
        $args = [
            'labels'             => $labels,
            'description'        => __('Description.', 'got-soccer'),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => ['slug' => 'club'],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 93,
            'supports'           => ['title', 'editor', 'custom-fields']
        ];
        register_post_type('got-club', $args);
    }

    private function createMatches()
    {
        $labels = [
            'name'               => _x('Matches', 'post type general name', 'got-soccer'),
            'singular_name'      => _x('Match', 'post type singular name', 'got-soccer'),
            'menu_name'          => _x('Matches', 'admin menu', 'got-soccer'),
            'name_admin_bar'     => _x('Match', 'add new on admin bar', 'got-soccer'),
            'add_new'            => _x('Add New', 'league', 'got-soccer'),
            'add_new_item'       => __('Add New Match', 'got-soccer'),
            'new_item'           => __('New Match', 'got-soccer'),
            'edit_item'          => __('Edit Match', 'got-soccer'),
            'view_item'          => __('View Match', 'got-soccer'),
            'all_items'          => __('All Matches', 'got-soccer'),
            'search_items'       => __('Search Matches', 'got-soccer'),
            'parent_item_colon'  => __('Parent Matches:', 'got-soccer'),
            'not_found'          => __('No matches found.', 'got-soccer'),
            'not_found_in_trash' => __('No matches found in Trash.', 'got-soccer')
        ];
        $args = [
            'labels'             => $labels,
            'description'        => __('Description.', 'got-soccer'),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => ['slug' => 'match'],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 94,
            'supports'           => ['title', 'editor', 'custom-fields']
        ];
        register_post_type('got-match', $args);
    }

    private function createTeams()
    {
        $labels = [
            'name'               => _x('Teams', 'post type general name', 'got-soccer'),
            'singular_name'      => _x('Team', 'post type singular name', 'got-soccer'),
            'menu_name'          => _x('Teams', 'admin menu', 'got-soccer'),
            'name_admin_bar'     => _x('Team', 'add new on admin bar', 'got-soccer'),
            'add_new'            => _x('Add New', 'league', 'got-soccer'),
            'add_new_item'       => __('Add New Team', 'got-soccer'),
            'new_item'           => __('New Team', 'got-soccer'),
            'edit_item'          => __('Edit Team', 'got-soccer'),
            'view_item'          => __('View Team', 'got-soccer'),
            'all_items'          => __('All Teams', 'got-soccer'),
            'search_items'       => __('Search Teams', 'got-soccer'),
            'parent_item_colon'  => __('Parent Teams:', 'got-soccer'),
            'not_found'          => __('No teams found.', 'got-soccer'),
            'not_found_in_trash' => __('No teams found in Trash.', 'got-soccer')
        ];
        $args = [
            'labels'             => $labels,
            'description'        => __('Description.', 'got-soccer'),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => ['slug' => 'team'],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 95,
            'supports'           => ['title', 'editor', 'custom-fields']
        ];
        register_post_type('got-team', $args);
    }

    public static function registerPostType($slug, $singular, $plural, $args = [])
    {
        $labels = [
            'name'               => $plural,
            'singular_name'      => $singular,
            'menu_name'          => $plural,
            'name_admin_bar'     => $singular,
            'add_new'            => sprintf(__('Add %1$s', 'storymate'), $singular),
            'add_new_item'       => sprintf(__('Add New %1$s', 'storymate'), $singular),
            'new_item'           => sprintf(__('New %1$s', 'storymate'), $singular),
            'edit_item'          => sprintf(__('Edit %1$s', 'storymate'), $singular),
            'view_item'          => sprintf(__('View %1$s', 'storymate'), $plural),
            'all_items'          => sprintf(__('%1$s', 'storymate'), $plural),
            'search_items'       => sprintf(__('search %1$s', 'storymate'), $plural),
            'parent_item_colon'  => sprintf(__('Parent %1$s:', 'storymate'), $plural),
            'not_found'          => sprintf(__('No %1$s found.', 'storymate'), $plural),
            'not_found_in_trash' => sprintf(__('No %1$s found in Trash.', 'storymate'), $plural)
        ];

        $defaults = [
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => [ 'title', 'editor', 'thumbnail' ],
            'menu_icon'          => 'dashicons-admin-site'
        ];

        register_post_type($slug, wp_parse_args($args, $defaults));
    }

    public function createTables()
    {
        global $wpdb;

        $table_prefix = $wpdb->prefix.$this->prefix;
        $charset      = $wpdb->get_charset_collate();

        $create_events_table   = $this->createEventsTable($table_prefix, $charset);
        $create_clubs_table    = $this->createClubsTable($table_prefix, $charset);
        $create_schedule_table = $this->createScheduleTable($table_prefix, $charset);
        $create_teams_table    = $this->createTeamsTable($table_prefix, $charset);
    }

    private function createEventsTable($prefix, $charset)
    {

        if (!$prefix || !$charset) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}events` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `event_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `event_name` text,
          `event_type` text,
          `start_date` text,
          `end_date` text,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        dbDelta($sql);
    }

    private function createClubsTable($prefix, $charset)
    {

        if (!$prefix || !$charset) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}clubs` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `club_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `club_name` text,
          `club_logo_url` text,
          `club_website_url` text,
          `city` text,
          `state` text,
          `zip_code` text,
          `district` text,
          `total_teams` mediumint(9) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY  (id)
        ) $charset_collate;";

        dbDelta($sql);
    }

    private function createScheduleTable($prefix, $charset)
    {

        if (!$prefix || !$charset) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}schedule` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `event_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `event_name` text,
          `event_type` text,
          `start_date` text,
          `end_date` text,
          `group_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `gender` text,
          `group_age` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `group_name` text,
          `group_priority` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `bracket_name` text,
          `match_id` int(11) unsigned NOT NULL DEFAULT '0',
          `event_match_number` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `match_description` text,
          `event_date` text,
          `match_start_time` text,
          `match_end_time` text,
          `match_schedule_status` text,
          `home_team_description` text,
          `home_application_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `home_club_id` smallint(5) unsigned NOT NULL DEFAULT '0'
          `home_team_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `home_club_logo_url` text,
          `home_club_name` text,
          `home_team_name` text,
          `home_team_state` text,
          `home_team_name_full` text,
          `home_final_score` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `away_team_description` text,
          `away_application_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `away_club_id` smallint(5) unsigned NOT NULL DEFAULT '0'
          `away_team_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `away_club_logo_url` text,
          `away_club_name` text,
          `away_team_name` text,
          `away_team_state` text,
          `away_team_name_full` text,
          `away_final_score` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `field_location` text,
          `field_number` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `field_address` text,
          `field_city` text,
          `field_state` text,
          `field_zip` text,
          `field_image_url` text,
          `field_latitude` text,
          `field_longitude` text,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        dbDelta($sql);
    }

    private function createTeamsTable($prefix, $charset)
    {

        if (!$prefix || !$charset) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}teams` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `event_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `event_name` text,
          `event_type` text,
          `start_date` text,
          `end_date` text,
          `group_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `gender` text,
          `group_age` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `group_name` text,
          `group_priority` tinyint(2) unsigned NOT NULL DEFAULT '0',
          `bracket_id` text,
          `bracket_name` text,
          `club_id` smallint(5) unsigned NOT NULL DEFAULT '0'
          `club_logo_url` text,
          `application_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `team_id` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `club_name` text,
          `team_name` text,
          `team_state` text,
          `team_name_full` text,
          `team_yellow_cards` smallint(3) unsigned NOT NULL DEFAULT '0',
          `team_red_cards` smallint(3) unsigned NOT NULL DEFAULT '0',
          `group_team_seed_points` mediumint(9) unsigned NOT NULL DEFAULT '0',
          `group_team_number` smallint(3) unsigned NOT NULL DEFAULT '0',
          `bracket_team_number` smallint(3) unsigned NOT NULL DEFAULT '0',
          `group_play_placement` smallint(3) unsigned NOT NULL DEFAULT '0',
          `matches_played` smallint(3) unsigned NOT NULL DEFAULT '0',
          `wins` smallint(3) unsigned NOT NULL DEFAULT '0',
          `losses` smallint(3) unsigned NOT NULL DEFAULT '0',
          `draws` smallint(3) unsigned NOT NULL DEFAULT '0',
          `goals_for` smallint(3) unsigned NOT NULL DEFAULT '0',
          `goals_against` smallint(3) unsigned NOT NULL DEFAULT '0',
          `group_play_points` smallint(3) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY  (id)
        ) $charset_collate;";

        dbDelta($sql);
    }
}
