<?php

namespace GotSoccer\App\Core;

/**
 * Interface Hooks provides a contract for classes that add WordPress hooks
 *
 * @package GotSoccer\App\Core
 */
interface WordPressHooks
{
    public function addHooks();
}
