<?php

namespace GotSoccer\App\Sync;

use GotSoccer\App\Sync;
use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Clubs
 *
 * @package GotSoccer\App\Sync
 */
class Clubs extends Sync implements WordPressHooks
{
    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        // add_action('wp_ajax_got_soccer_sync_events', [$this, 'syncEventsAjax']);
    }

    public function syncClubs()
    {
        $get_clubs = $this->Api->GetClubs();

        if (!$get_clubs) {
            return false;
        }

        $total = isset($get_clubs->GetClubsSummary->TotalRows) && !empty((array) $get_clubs->GetClubsSummary->TotalRows) ? $get_clubs->GetClubsSummary->TotalRows : '-1';
        $count = $this->buildClubs($get_clubs->GetClubsResults->GetClubsResult);

        echo sprintf('Clubs Updated: %1$s/%2$s', $count, $total);
    }

    private function buildClubs($clubs)
    {
        if (empty($clubs)) {
            return false;
        }

        $count = 0;

        foreach ($clubs as $club) {
            $club_id = !empty((array) $club->ClubID) ? $club->ClubID : null;
            if (!$club_id) {
                continue;
            }

            $post_type = 'got-club';
            $post_name = !empty((array) $club->ClubName) ? $club->ClubName : null;
            $post_id   = $this->existingRowHandler($post_type, 'club_id', $club_id);
            $post_meta = $this->createPostMeta($club);
            $insert_id = $this->insertPost($post_id, $post_name, $post_type, $club_id, $post_meta);

            if (!empty($insert_id)) {
                $count++;
            }
        }

        update_option('gotsoccer_sync_clubs', time(), false);
        return $count;
    }
}
