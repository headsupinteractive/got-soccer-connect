'use strict';

// Load plugins
const gulp         = require('gulp');
const concat       = require('gulp-concat');
const rename       = require('gulp-rename');
const terser       = require('gulp-terser');
const sourcemaps   = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const sass         = require('gulp-sass');
const sassLint     = require('gulp-sass-lint');
const eslint       = require('gulp-eslint');
const phpcs        = require('gulp-phpcs');
const phpcbf       = require('gulp-phpcbf');
const gutil        = require('gulp-util');

// File paths to various assets are defined here.
const PATHS = {
  php: [
    '**/*.php',
    '!vendor/**/*.*'
  ],
  sass: [
    'assets/scss/*.scss',
    'assets/scss/**/*.scss'
  ],
  js: [
    'assets/js/admin/*.js',
    'assets/js/main.js'
  ]
};

// Concatenate & Minify all javascript files
function buildScripts() {
  return (
    gulp
      .src(PATHS.js)
      .pipe(concat('build.js'))
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(terser())
      .pipe(gulp.dest('build/js'))
  );
}

// Concatenate all theme javascript files
function devScripts() {
  return (
    gulp
      .src(PATHS.js)
      .pipe(concat('build.js'))
      .pipe(gulp.dest('build/js'))
  );
}

// run JS lint on theme scripts
function scriptsLint() {
  return gulp
    .src(PATHS.js)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

// Compile min CSS
function buildStyles() {
  return (
    gulp
      .src('assets/scss/main.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({
        includePaths: PATHS.sass,
        outputStyle: 'compressed'
      }))
      .pipe(autoprefixer())
      .pipe(rename({
        basename: 'build',
        suffix: '.min'
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('build/css'))
  );
}

// run SCSS lint on theme styles
function stylesLint() {
  return (
    gulp
      .src(PATHS.sass)
      .pipe(sassLint())
      .pipe(sassLint.format())
      .pipe(sassLint.failOnError())
  );
}

// run php code sniffer on theme files
function phpCodeSniffer() {
  return (
    gulp
      .src(PATHS.php)
      .pipe(phpcs({
        bin: 'vendor/squizlabs/php_codesniffer/bin/phpcs',
        standard: 'PSR2',
        warningSeverity: 0,
        colors: 1
      }))
      // Log all problems that was found
      .pipe(phpcs.reporter('log'))
  );
}

// run php code sniffer on theme files
function phpCodeBeautifier() {
  return (
    gulp
      .src(PATHS.php)
      .pipe(phpcbf({
        bin: './vendor/squizlabs/php_codesniffer/bin/phpcbf',
        standard: 'PSR2',
        warningSeverity: 0
      }))
      .on('error', gutil.log)
      .pipe(gulp.dest('.'))
  );
}

// Watch files
function watchDevFiles() {
  gulp.watch(PATHS.sass, gulp.series(stylesLint, buildStyles));
  gulp.watch(PATHS.js, gulp.series(scriptsLint, devScripts));
  gulp.watch(PATHS.php, phpCodeSniffer);
}

// Watch files
function watchFiles() {
  gulp.watch(PATHS.sass, gulp.series(stylesLint, buildStyles));
  gulp.watch(PATHS.js, gulp.series(scriptsLint, buildScripts));
  gulp.watch(PATHS.php, phpCodeSniffer);
}

// define complex tasks
const js = gulp.series(scriptsLint, gulp.parallel(buildScripts));
const build = gulp.series(scriptsLint, stylesLint, gulp.parallel(phpCodeSniffer, buildScripts, buildStyles));
const dev = gulp.series(scriptsLint, stylesLint, gulp.parallel(phpCodeSniffer, devScripts, buildStyles));

// export tasks
exports.scripts     = buildScripts;
exports.scriptsDev  = devScripts;
exports.scriptsLint = scriptsLint;
exports.styles      = buildStyles;
exports.sassLint    = stylesLint;
exports.phpcs       = phpCodeSniffer;
exports.beautify    = phpCodeBeautifier;
exports.watchDev    = watchDevFiles;
exports.watch       = watchFiles;
exports.js          = js;
exports.build       = build;
exports.dev         = dev;
exports.default     = build;