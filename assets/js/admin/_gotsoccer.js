const gotsoccer = (function ($) {

  const pub           = {};
  const syncForm      = $('#gotsoccersync');
  const gotEventsRow  = $('#syncEventsRow');
  const gotClubsRow   = $('#syncClubsRow');
  const gotMatchesRow = $('#syncMatchesRow');
  const gotTeamsRow   = $('#syncTeamsRow');

  pub.init = function () {
    pub._syncAll();
  };

  pub._syncAll = function() {
    syncForm.submit(function(e) {
      e.preventDefault();
      pub._disableForm(syncForm);
      pub._showSpinner(syncForm.find('.spinner'));
      console.log('start sync');

      pub._eventSync().then(pub._clubSync());
    });
  };

  pub._eventSync = function() {
    gotEventsRow.addClass('syncing');
    return new Promise(function (resolve, reject) {
      pub._gotSyncData({
        action: 'got_soccer_sync_events'
      }, resolve, reject);
    });
  };

  pub._clubSync = function() {
    gotClubsRow.addClass('syncing');
    return new Promise(function (resolve, reject) {
      pub._gotSyncData({
        action: 'got_soccer_sync_clubs'
      }, resolve, reject);
    });
  };

  pub._matchSync = function() {
    gotMatchesRow.addClass('syncing');
    return new Promise(function (resolve, reject) {
      pub._gotSyncData({
        action: 'got_soccer_sync_matches'
      }, resolve, reject);
    });
  };

  pub._teamSync = function() {
    gotTeamsRow.addClass('syncing');
    console.log('end sync');
    // return new Promise(function (resolve, reject) {
    //   gotSyncData({
    //     action: 'got_soccer_sync_teams'
    //   }, resolve, reject);
    // });
  };

  pub._disableForm = function(form) {
    if (!form) {
      return false;
    }

    form.find('input[type=submit]').prop('disabled', true);
  };

  pub._showSpinner = function(spinner) {
    if (!spinner) {
      return false;
    }

    spinner.addClass('is-active');
  };

  pub._gotSyncData = function(formData, resolve, reject) {
    $.ajax({
      type: 'POST',
      url: ajaxurl,
      data: formData,
      success: function(data, response) {
        resolve();
        return data;
      },
      error: function(data, response) {
        reject();
        return false;
      }
    }).done(function (data, response) {
      console.log(data);
    });
  };

  return pub;

})(jQuery);
