<?php

namespace GotSoccer\App;

use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Scripts
 *
 * @package GotSoccer\App
 */
class Scripts implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueueScripts']);
    }

    /**
     * Load scripts for the back end.
     */
    public function enqueueScripts()
    {
        wp_enqueue_script(
            'got-soccer-js',
            GOTSOCCER_API_URL . 'build/js/build.min.js',
            ['jquery'],
            GOTSOCCER_VERSION,
            true
        );

        wp_enqueue_style(
            'got-soccer-styles',
            GOTSOCCER_API_URL . 'build/css/build.min.css',
            [],
            GOTSOCCER_VERSION
        );
    }
}
