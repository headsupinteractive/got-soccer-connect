<?php

namespace GotSoccer\App\Sync;

use GotSoccer\App\Sync;
use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Matches
 *
 * @package GotSoccer\App\Sync
 */
class Events extends Sync implements WordPressHooks
{
    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        // add_action('wp_ajax_got_soccer_sync_events', [$this, 'syncEventsAjax']);
    }

    public function syncEvents()
    {
        $get_events = $this->Api->GetEvents();

        if (!$get_events) {
            return false;
        }

        $total = isset($get_events->GetEventsSummary->TotalRows) && !empty((array) $get_events->GetEventsSummary->TotalRows) ? $get_events->GetEventsSummary->TotalRows : '-1';
        $count = $this->buildEvents($get_events->GetEventsResults->GetEventsResult);

        echo sprintf('Events Updated: %1$s/%2$s', $count, $total);
    }

    private function buildEvents($events)
    {
        if (empty($events)) {
            return false;
        }

        $count = 0;

        foreach ($events as $event) {
            $event_id   = !empty((array) $event->EventID) ? $event->EventID : false;
            $event_type = !empty((array) $event->EventType) ? $event->EventType : false;
            
            if (!$event_id || !$event_type) {
                continue;
            }

            switch (strtolower($event_type)) {
                case 'league':
                    $post_type = 'got-league';
                    break;
                case 'tournament':
                    $post_type = 'got-tournament';
                    break;
                default:
                    $post_type = false;
            }

            if ($post_type === false) {
                continue;
            }

            $post_name = !empty((array) $event->EventName) ? $event->EventName : null;
            $post_id   = $this->existingRowHandler($post_type, 'event_id', $event_id);
            $post_meta = $this->createPostMeta($event);
            $insert_id = $this->insertPost($post_id, $post_name, $post_type, $event_id, $post_meta);

            if (!empty($insert_id)) {
                $count++;
            }
        }

        update_option('gotsoccer_sync_events', time(), false);
        return $count;

        die();
    }
}
