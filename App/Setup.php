<?php

namespace GotSoccer\App;

use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Setup
 *
 * @package GotSoccer\App
 */
class Setup implements WordPressHooks
{
    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('admin_menu', [$this, 'adminMenu']);
    }

    public function adminMenu()
    {
        add_menu_page('GotSoccer Sync', 'GotSoccer Sync', 'manage_options', 'got-soccer-connect', [$this, 'adminSyncPage'], '', 90);
        add_submenu_page('got-soccer-connect', 'Sync Settings', 'Sync Settings', 'manage_options', 'got-soccer-settings', [$this, 'adminSettingsPage']);
    }

    public function adminSyncPage()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'got-soccer'));
        }

        require_once GOTSOCCER_API_DIR . '/templates/sync.php';
    }

    public function adminSettingsPage()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'got-soccer'));
        }

        require_once GOTSOCCER_API_DIR . '/templates/settings.php';
    }
}
