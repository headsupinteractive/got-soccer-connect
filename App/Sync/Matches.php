<?php

namespace GotSoccer\App\Sync;

use GotSoccer\App\Sync;
use GotSoccer\App\Core\WordPressHooks;

/**
 * Class Matches
 *
 * @package GotSoccer\App\Sync
 */
class Matches extends Sync implements WordPressHooks
{
    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        // add_action('wp_ajax_got_soccer_sync_events', [$this, 'syncEventsAjax']);
    }

    public function syncMatches()
    {
        $page_size = 1000;

        $get_matches = $this->Api->GetEventSchedule(
            $EventID = null,
            $GroupID = null,
            $BracketID = null,
            $ClubID = null,
            $ApplicationID = null,
            $TeamID = null,
            $FieldID = null,
            $MatchID = null,
            $Gender = null,
            $AgeGroup = null,
            $FromDate = null,
            $ToDate = null,
            $PageSize = $page_size,
            $Page = 1
        );

        if (!$get_matches) {
            return false;
        }

        $log = $this->startLog('gotsoccer-matches.log', 'w+');
        fwrite($log, "Page: 1\r\n");

        // Get total pages
        $pages = isset($get_matches->GetEventScheduleSummary->TotalPages) ? (int) $get_matches->GetEventScheduleSummary->TotalPages : 1;
        $total = isset($get_matches->GetEventScheduleSummary->TotalRows) ? (int) $get_matches->GetEventScheduleSummary->TotalRows : '-1';
        $updated = $this->buildMatches($get_matches->GetEventScheduleResults->GetEventScheduleResult, $log);

        if ($total <= $page_size) {
            echo sprintf('Teams Updated: %1$s/%2$s', $updated, $total);
            return;
        }

        // Loop for each page
        for ($i = 2; $i <= $pages; $i++) {
            $get_matches = $this->Api->GetEventSchedule(
                $EventID = null,
                $GroupID = null,
                $BracketID = null,
                $ClubID = null,
                $ApplicationID = null,
                $TeamID = null,
                $FieldID = null,
                $MatchID = null,
                $Gender = null,
                $AgeGroup = null,
                $FromDate = null,
                $ToDate = null,
                $PageSize = $page_size,
                $Page = $i
            );

            fwrite($log, "\r\nPage: ".$i."\r\n");
            $updated += $this->buildMatches($get_matches->GetEventScheduleResults->GetEventScheduleResult, $log);
        }

        echo sprintf('Matches Updated: %1$s/%2$s', $updated, $total);
    }

    private function buildMatches($matches, $log)
    {
        if (empty($matches)) {
            return false;
        }

        $count = 0;

        foreach ($matches as $match) {
            set_time_limit(0);
            
            $match_id   = isset($match->MatchID) ? $match->MatchID : false;
            $match_type = isset($match->EventType) && !empty((array) $match->EventType) ? strtolower($match->EventType) : false;

            if ($match_type !== 'league') {
                fwrite($log, $match_id . ": Skipped\r\n");
                continue;
            }

            $match_date      = isset($match->EventDate) ? $match->EventDate : false;
            $match_away_team = isset($match->AwayTeamNameFull) ? $match->AwayTeamNameFull : false;
            $match_home_team = isset($match->HomeTeamNameFull) ? $match->HomeTeamNameFull : false;
            
            if (!$match_id || !$match_date || !$match_away_team || !$match_home_team) {
                continue;
            }

            $post_type = 'got-match';
            $post_name = $match_date . ' - ' . $match_away_team . ' @ ' . $match_home_team;
            $post_id   = $this->existingRowHandler($post_type, 'match_id', $match_id);
            $post_meta = $this->createPostMeta($match);
            $insert_id = $this->insertPost($post_id, $post_name, $post_type, $match_id, $post_meta);

            if (!is_wp_error($insert_id)) {
                fwrite($log, $match_id.": ".$post_name."\r\n");
                $count++;
            } else {
                fwrite($log, $match_id.": ".$post_name." (error) ".json_encode($insert_id)."\r\n");
            }
        }

        update_option('gotsoccer_sync_matches', time(), false);
        return $count;
    }
}
