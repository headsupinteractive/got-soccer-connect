<?php
date_default_timezone_set('America/Chicago');

$sync_all      = get_option('gotsoccer_sync_all');
$sync_all      = !empty($sync_all) ? $sync_all : null;
$sync_all_time = !empty($sync_all) ? date('M j, Y @ g:i A', $sync_all) : 'Never';

$sync_events        = get_option('gotsoccer_sync_events');
$sync_events        = !empty($sync_events) ? $sync_events : null;
$sync_events_status = !empty($sync_events) ? 'success' : 'warning';
$sync_events_time   = !empty($sync_events) ? date('M j, Y @ g:i A', $sync_events) : 'Never';

$sync_clubs        = get_option('gotsoccer_sync_clubs');
$sync_clubs        = !empty($sync_clubs) ? $sync_clubs : null;
$sync_clubs_status = !empty($sync_clubs) ? 'success' : 'warning';
$sync_clubs_time   = !empty($sync_clubs) ? date('M j, Y @ g:i A', $sync_clubs) : 'Never';

$sync_matches        = get_option('gotsoccer_sync_matches');
$sync_matches        = !empty($sync_matches) ? $sync_matches : null;
$sync_matches_status = !empty($sync_matches) ? 'success' : 'warning';
$sync_matches_time   = !empty($sync_matches) ? date('M j, Y @ g:i A', $sync_matches) : 'Never';

$sync_teams        = get_option('gotsoccer_sync_teams');
$sync_teams        = !empty($sync_teams) ? $sync_teams : null;
$sync_teams_status = !empty($sync_teams) ? 'success' : 'warning';
$sync_teams_time   = !empty($sync_teams) ? date('M j, Y @ g:i A', $sync_teams) : 'Never';

$sync_next_update = (date('H') < 12) ? date('M j, Y @') . ' 12:00 PM' : date('M j, Y @', time() + 86400) . ' 12:00 AM';

?>

<div class="wrap got-soccer-wrap">
    <h1><?php _e('GotSoccer Sync', 'got-soccer'); ?></h1>
    <form id="gotsoccersync" method="post" name="post" class="got-soccer-form">
        <input type="hidden" name="action" value="got_soccer_sync" />
        <div id="poststuff">
            <div id="post-body" class="metabox-holder columns-2">
                <div id="postbox-container-2" class="postbox-container">
                    <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                        <div class="postbox">
                            <button type="button" class="handlediv" aria-expanded="true">
                                <span class="screen-reader-text"><?php _e('Toggle panel: Sync Status', 'got-soccer'); ?></span>
                                <span class="toggle-indicator" aria-hidden="true"></span>
                            </button>
                            <h2 class="hndle ui-sortable-handle"><span><?php _e('API Endpoints - Sync Status', 'got-soccer'); ?></span></h2>
                            <div class="inside">
                                <table class="form-table">
                                    <tbody>
                                        <tr id="syncEventsRow" class="sync-row sync-<?php echo esc_html($sync_events_status); ?>">
                                            <th scope="row">
                                                <label for="orgid"><?php _e('Events', 'got-soccer'); ?></label>
                                                <span class="sync-time"><?php echo __('Last Sync:', 'got-soccer') . ' ' . esc_html($sync_events_time); ?></span>
                                            </th>
                                            <td>
                                                <button id="btnSyncEvents" class="button button-got-sync"><?php _e('Sync', 'got-soccer'); ?></button>
                                                <span class="sync-status">
                                                    <span class="dashicons dashicons-yes"></span>
                                                    <span class="dashicons dashicons-warning"></span>
                                                    <span class="spinner"></span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="syncClubsRow" class="sync-row sync-<?php echo esc_html($sync_clubs_status); ?>">
                                            <th scope="row">
                                                <label for="login"><?php _e('Clubs', 'got-soccer'); ?></label>
                                                <span class="sync-time"><?php echo __('Last Sync:', 'got-soccer') . ' ' . esc_html($sync_clubs_time); ?></span>
                                            </th>
                                            <td>
                                                <button id="btnSyncClubs" class="button button-got-sync"><?php _e('Sync', 'got-soccer'); ?></button>
                                                <span class="sync-status">
                                                    <span class="dashicons dashicons-yes"></span>
                                                    <span class="dashicons dashicons-warning"></span>
                                                    <span class="spinner"></span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="syncMatchesRow" class="sync-row sync-<?php echo esc_html($sync_matches_status); ?>">
                                            <th scope="row">
                                                <label for="pass"><?php _e('Matches', 'got-soccer'); ?></label>
                                                <span class="sync-time"><?php echo __('Last Sync:', 'got-soccer') . ' ' . esc_html($sync_matches_time); ?></span>
                                            </th>
                                            <td>
                                                <button id="btnSyncMatches" class="button button-got-sync"><?php _e('Sync', 'got-soccer'); ?></button>
                                                <span class="sync-status">
                                                    <span class="dashicons dashicons-yes"></span>
                                                    <span class="dashicons dashicons-warning"></span>
                                                    <span class="spinner"></span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="syncTeamsRow" class="sync-row sync-<?php echo esc_html($sync_teams_status); ?>">
                                            <th scope="row">
                                                <label for="pass"><?php _e('Teams', 'got-soccer'); ?></label>
                                                <span class="sync-time"><?php echo __('Last Sync:', 'got-soccer') . ' ' . esc_html($sync_teams_time); ?></span>
                                            </th>
                                            <td>
                                                <button id="btnSyncTeams" class="button button-got-sync"><?php _e('Sync', 'got-soccer'); ?></button>
                                                <span class="sync-status">
                                                    <span class="dashicons dashicons-yes"></span>
                                                    <span class="dashicons dashicons-warning"></span>
                                                    <span class="spinner"></span>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="postbox-container-1" class="postbox-container">
                    <div id="side-sortables" class="meta-box-sortables ui-sortable">
                        <div id="submitdiv" class="postbox">
                            <button type="button" class="handlediv" aria-expanded="true">
                                <span class="screen-reader-text"><?php _e('Toggle panel: Publish', 'got-soccer'); ?></span>
                                <span class="toggle-indicator" aria-hidden="true"></span>
                            </button>
                            <h2 class="hndle ui-sortable-handle"><span><?php _e('Sync All Data', 'got-soccer'); ?></span></h2>
                            <div class="inside">
                                <div id="misc-publishing-actions">
                                    <div class="misc-pub-section misc-pub-sync-status">
                                        <strong><?php _e('Last Update', 'got-soccer'); ?>: </strong><?php echo esc_html($sync_all_time); ?><br><br>
                                        <strong><?php _e('Next Sync', 'got-soccer'); ?>: </strong><?php echo esc_html($sync_next_update); ?><br><br>
                                    </div>
                                </div>
                                <div id="major-publishing-actions">
                                    <div id="publishing-action">
                                        <span class="spinner"></span>
                                        <input type="submit" accesskey="p" value="Sync All" class="button button-primary button-large" id="publish" name="publish">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>
    </form>
    <div style="display:inline-block;width:100%;">
    <?php
    use GotSoccer\App\Sync;
    $Sync = new Sync();

    $Sync->syncAll();
    ?>
    </div>
</div>